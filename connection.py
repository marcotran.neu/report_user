import pandas as pd
import os
import psycopg2


def connection_postgres():
    conn = psycopg2.connect(host=,
                            database=,
                            user=,
                            password=,
                            port=5432
                            )
    return conn


def get_current( query):
    # This is a function that connects to the database, runs a query, and returns the results as a dataframe.
    conn = connection_postgres()
    df = pd.read_sql(sql=query, con=conn)
    conn.close()
    return df
