from connection import *
import streamlit as st


def get_list_user():
    query = "SELECT * FROM account_services.users;"
    data = get_current(query)
    return data


def get_detail_by_username(username):
    try:
        query = f"""with map_info_type as(
                select * from account_services.info_types it
                left join account_services.user_infos ui on ui.type_id = it.id)
                select u.username,mit.info_type, mit.info from account_services.users u
            left join map_info_type mit on u.id = mit.user_id where u.username = '{username}'"""
        data = get_current(query)
        return data
    except:
        return None

tab1, tab2 = st.tabs(["All", "Details"])
with tab1:
    if st.button('All user'):
        st.dataframe(get_list_user())

with tab2:
    input_username = st.text_input(
        "Enter username 👇")
    if input_username:
        st.dataframe(get_detail_by_username(input_username))


